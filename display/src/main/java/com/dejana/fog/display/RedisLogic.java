package com.dejana.fog.display;

import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Component
public class RedisLogic {
    private Jedis jedis = new Jedis("redis");
    private String prevTime;
    private Map<String, Double> map = new LinkedHashMap<>();

    Map<String, Double> getTopResults() {
        String newTime = jedis.get("last-modified-time");

        if (prevTime == null || !prevTime.equals(newTime)) {
            map = new LinkedHashMap<>();

            Set<Tuple> ranking = jedis.zrevrangeWithScores("ranking", 0, 10);
            for (Tuple t : ranking) {
                map.put(t.getElement(), t.getScore());
            }
            System.out.println("Reading from redis.");


        } else {
            System.out.println("Reading from cache.");
        }
        prevTime = newTime;

        return map;
    }

}
