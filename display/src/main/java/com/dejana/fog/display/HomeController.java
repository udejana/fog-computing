package com.dejana.fog.display;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class HomeController {
    @Autowired
    private RedisLogic redisLogic;

    @GetMapping("/display")
    public Map<String, Double> display() {

        return redisLogic.getTopResults();
    }
}
