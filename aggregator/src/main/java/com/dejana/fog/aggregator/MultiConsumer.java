package com.dejana.fog.aggregator;

import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MultiConsumer {
    private List<Consumer> consumers = new ArrayList<>();
    Jedis jedis = new Jedis("redis");

    public MultiConsumer(String... servers) {
        for (int i = 0; i < servers.length; i++) {
            consumers.add(new Consumer(servers[i]));
        }
    }

    public void initialize() {
        for (Consumer consumer : consumers) {
            new Thread(consumer::startListening).start();
        }
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(20000);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                aggregate();
            }
        }).start();
    }

    private void aggregate() {
        Map<String, Long> aggregateMap = new HashMap<>();
        for (Consumer consumer : consumers) {
            Map<String, Long> wordCounts = consumer.getWordCounts();
            wordCounts.forEach((k, v) -> aggregateMap.merge(k, v, (i1, i2) -> i1 + i2));
        }

        try {
            Thread.sleep(10000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        aggregateMap.forEach((k, v) -> {
            jedis.zadd("ranking", v, k);

        });
        jedis.set("last-modified-time", String.valueOf(System.currentTimeMillis()));


    }

}
