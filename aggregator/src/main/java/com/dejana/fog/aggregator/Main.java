package com.dejana.fog.aggregator;

public class Main {
    private static final String KAFKA_SERVER = "KAFKA_SERVER";

    public static void main(String[] args) {


        String[] servers;
        String server;
        if (args.length > 0 && args[0] != null) {
            server = args[0];
        } else if (System.getenv(KAFKA_SERVER) != null) {
            server = System.getenv(KAFKA_SERVER);
        } else {
            throw new IllegalArgumentException("Kafka server not provided.");
        }
        servers = server.split(",");
        MultiConsumer mc = new MultiConsumer(servers);
        mc.initialize();

    }
}
