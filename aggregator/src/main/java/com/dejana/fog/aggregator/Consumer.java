package com.dejana.fog.aggregator;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class Consumer {

    private final String server;
    private Map<String, Long> wordCounts = new ConcurrentHashMap<>();

    public Consumer(String server) {
        this.server = server;
    }

    public Map<String, Long> getWordCounts() {
        return wordCounts;
    }

    public void startListening() {
        // org.apache.log4j.BasicConfigurator.configure();
        KafkaConsumer<String, Long> consumer;

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", server);
        properties.setProperty("group.id", "test");
        properties.setProperty("enable.auto.commit", "true");
        properties.setProperty("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.setProperty("value.deserializer", "org.apache.kafka.common.serialization.LongDeserializer");
        properties.setProperty("session.timeout.ms", "10000");
        properties.setProperty("fetch.min.bytes", "50000");
        properties.setProperty("receive.buffer.bytes", "262144");
        properties.setProperty("max.partition.fetch.bytes", "2097152");

        consumer = new KafkaConsumer<String, Long>(properties);

        consumer.subscribe(Arrays.asList("word-count-output"));
        while (true) {
            try {
                ConsumerRecords<String, Long> records = consumer.poll(200);
                for (ConsumerRecord<String, Long> record : records) {
                    wordCounts.put(record.key(), record.value());

                    System.out.println("[" + server + "] " + "key: " + record.key() + ", value: " + record.value());
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                System.err.println(e);
            }
        }
    }
}
