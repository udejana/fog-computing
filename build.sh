# Example usage
# ./build.sh aggregator/ client-simulator/ stream-processor/ display/
# This will build the code for all the projects and create corresponding docker images

#!/usr/bin/env bash
for var in "$@"
do
    cd "$var"
    mvn clean package docker:build
    cd ..
done