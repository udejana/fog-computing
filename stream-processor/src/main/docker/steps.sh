#!/usr/bin/env bash

docker exec -it kafka2 /bin/bash

/opt/kafka_2.11-0.10.1.0/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 2 --topic word-count-input
/opt/kafka_2.11-0.10.1.0/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 2 --topic word-count-output

bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 \
     --topic word-count-output \
     --from-beginning \
     --formatter kafka.tools.DefaultMessageFormatter \
     --property print.key=true \
     --property print.value=true \
     --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
     --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer


/opt/kafka_2.11-0.10.1.0/bin/kafka-console-consumer.sh --zookeeper 10.5.0.6:2181 --topic word-count-output  --from-beginning