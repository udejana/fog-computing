package com.dejana.fog.streamprocessor;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class WordCounts {
    public static final String KAFKA_SERVER = "KAFKA_SERVER";

    public static void main(String[] args) {

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String server;
        List<String> stopWords = getStopWords();


        if (args.length > 0 && args[0] != null) {
            server = args[0];
        } else if (System.getProperty(KAFKA_SERVER) != null) {
            server = System.getProperty(KAFKA_SERVER);
        } else if (System.getenv(KAFKA_SERVER) != null) {
            server = System.getenv(KAFKA_SERVER);
        } else {
            throw new IllegalArgumentException("Kafka server not provided.");
        }

        System.out.println("Kafka server: " + server);

        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "word-counts");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, server);
        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 500);

        StreamsBuilder builder = new StreamsBuilder();
        //Stream from Kafka
        KStream<String, String> wordCountInput = builder.stream("word-count-input");


        //map values to lower case
        KTable<String, Long> wordCounts = wordCountInput
                //flatmap values split by space
                .flatMapValues(value -> Arrays.asList(value.split("\\W+")))
                .mapValues(value -> value.toLowerCase().replaceAll("[^a-zA-Z ]", ""))
                //drop stop words
                .filter(((key, value) -> !stopWords.contains(value) && value.length() > 2))
                //select key to apply a key (we discard old key (it was null anyway))
                .selectKey(((key, value) -> value))
                //group by key before aggregation
                .groupByKey()
                //count occurrences
                .count(Materialized.as("Counts"));
        //to in order to write result back to Kafka
        wordCounts.toStream().to("word-count-output", Produced.with(Serdes.String(), Serdes.Long()));

        KafkaStreams streams = new KafkaStreams(builder.build(), config);
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

        streams.start();
        System.out.println("**************************************");
        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));

    }

    private static List<String> getStopWords() {
        List<String> stopWords = new ArrayList<>();

        InputStream resourceAsStream = WordCounts.class.getClassLoader().getResourceAsStream("stop-words.txt");
        BufferedReader br = new BufferedReader(new InputStreamReader(resourceAsStream));
        String line;
        try {
            while ((line = br.readLine()) != null) {
                stopWords.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stopWords;
    }
}


