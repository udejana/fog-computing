package com.dejana.fog.clientsimulator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Run {
    private static final String KAFKA_SERVER = "KAFKA_SERVER";


    public static void main(String[] args) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<String> data = new ArrayList<>();
        Random random = new Random();
        for (int i = 1; i <= 13; i++) {
            data.add("text-source" + i + ".txt");
        }

        String[] servers;
        String server;
        if (args.length > 0 && args[0] != null) {
            server = args[0];
        } else if (System.getenv(KAFKA_SERVER) != null) {
            server = System.getenv(KAFKA_SERVER);
        } else {
            throw new IllegalArgumentException("Kafka server not provided.");
        }
        servers = server.split(",");
        while (true) {


            for (int i = 0; i < servers.length; i++) {
                String serverKafka = servers[i];
                String dataKafka = data.get(random.nextInt(10));
                System.out.println("Server:" + servers[i]);

                Thread t = new Thread(() -> {
                    try {
                        Producer.send(serverKafka, dataKafka);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                t.start();
            }
            try {
                Thread.sleep(120000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }
}
