package com.dejana.fog.clientsimulator;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class Producer {
    public static void send(String server, String bookName) throws IOException {

        KafkaProducer<String, String> producer;
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", server);
        properties.setProperty("acks", "all");
        properties.setProperty("retries", "0");
        properties.setProperty("batch.size", "16384");
        properties.setProperty("linger.ms", "0");
        properties.setProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.setProperty("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        producer = new KafkaProducer<>(properties);


        InputStream resourceAsStream = Producer.class.getClassLoader().getResourceAsStream(bookName);
        BufferedReader br = new BufferedReader(new InputStreamReader(resourceAsStream));
        String line;
        int counter = 0;
        while ((line = br.readLine()) != null) {

            ProducerRecord<String, String> pRecord = new ProducerRecord<String, String>("word-count-input", String.valueOf(counter), line);

            producer.send(pRecord);
            counter++;
            if (counter % 100 == 0) {
                producer.flush();
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Sent " + (counter * 100) + " messages");
            }

        }
        producer.close();

    }

}

